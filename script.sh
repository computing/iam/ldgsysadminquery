RED='\033[0;31m'
NC='\033[0m'

validkrb=`klist | grep "krbtgt/LIGO.ORG@LIGO.ORG"`
if [ -z "$validkrb" ];  then echo -e "${RED}A valid kerberos ticket does not exist. Please kinit to get a kerberos ticket from LIGO KDC first.${NC}"; exit 1; fi

read -p "Enter albert.einstein username you want to query: " username

validuser=`ldapsearch -H ldap://ldap.ligo.org -Z  -b 'dc=ligo,dc=org' uid=${username} | grep "employeeNumber"`

if [ -z "$validuser" ];  then echo -e "${RED}Check the user name. The user does not exist in the LDAP.${NC}"; exit 1; fi


read -p "Enter how much time back you want to check the membership. e.g. -1 day,  -1 month, or -1 year: " timeback

d=`date -d "${timeback}" '+%Y/%m/%d'`
java -jar grouperClient-2.6.9.jar  --operation=hasMemberWs  --groupName=Communities:LSCVirgoLIGOGroupMembers --subjectIdentifiers=${username}@LIGO.ORG --pointInTimeFrom=${d} 2> /dev/null
