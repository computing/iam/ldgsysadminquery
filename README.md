Here is how you can find out if a user was a member in -XX back in time. This will be useful if you want to determine whether to archive an LDG account if has been too long  (that your determine for your cluster) since the user left the collaboration.  You will need an installation of java and also you will need a password. 


The steps:

1) Download the script and package:

git clone git@git.ligo.org:computing/iam/ldgsysadminquery.git


2) cd ldgsysadminquery


3) You can see a file: grouper.client.properties

You can fill the password next to the following filed: 
grouperClient.webService.password =


You can obtain the password from here: https://secrets.ligo.org/secrets/477/

If you have problem accessing the password, let me know. 

4) Then you can simply run the script

$ ./script.sh 

Enter username: warren.anderson

Enter how much time back you want to check the membership. e.g. -1 day,  -1 month, or -1 year: -3 month

Index 0: success: T: code: IS_MEMBER: warren.anderson@LIGO.ORG: true



If one needs to check out the exact date the user left, one could look at the user Audit in grouper UI:
e.g. https://group-registry.ligo.org/grouper/grouperUi/app/UiV2Main.index?operation=UiV2Subject.viewAudits&subjectId=bruce.allen@LIGO.ORG&sourceId=ligo

Note https://my.ligo.org/whois_web.php could also achive that via a browser. 
